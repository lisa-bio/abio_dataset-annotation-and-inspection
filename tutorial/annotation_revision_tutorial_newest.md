# 实习生数据集标注以及修改指南

# 数据集标注指南

### 注：文献具有时效性，实习生若是在一周内无法完成分配的数据集，需要及时向管理人员汇报。进行数据标注，需要从data/tutorial/template_new/code/文件夹下复制script.ipynb代码文件模板到自己的文件夹下参考运行. 不可以直接在模板中运行！！！脚本会随时更新，请随时关注新加入的字段，不要一直使用自己存在文件夹下面的script，每次用都从template下复制最好。

#### 流程：
## 1. Report：审核人员下发任务到redmine——拿到数据集——进行标注——（有的数据集缺少部分内容，如tSNE，UMAP，cluster，markerGene等，需要在腾讯文档表格中登记。不清楚的请联系管理员）填写腾讯文档表格——继续标注完成——生成pdf版本的report——redmine回复完成并附件添加report——等待二审和修改 （腾讯文档链接：[https://docs.qq.com/sheet/DRmlhcE9EeXZ6dmFT](https://docs.qq.com/sheet/DRmlhcE9EeXZ6dmFT)）

每新标注一个数据集，都要把自己写的代码，每一个表格尽可能的展开，标注的代码需要整洁有序可读，把网页打印-选择生成pdf版，并把文件重新命名为report_数据集编号_用户名和名字_日期.pdf, 实习生直接贴在redmine任务平台相应任务下即可。

标注人员根据二审人员的修改意见进行修改时，请不要直接在原来的代码上运行。需要在检查器部分之后编写代码，同样需要代码整洁有序，方便审核人员查看所做改动。



## 2. unstructuredData需要摘录的信息：

#### title/abstract/sourceID/journal/publicationDate/authors/keywords/citation这几项现在可以调运函数自动生成了，但是需要检查生成的内容中是否有乱码。

#### 当某个字段没有内容可以填写时，若是字符串形式，空着即可；若是整数形式，0即可。

- subDataset：由一审人员写在description.txt上，请复制过来，注意part_n不一定等于SubDataset-n，part和subdataset的关系参见cellAnnotation部分
- description：对subDataset的描述。由一审人员写在description.txt上，请复制过来
- correspondingFigure：由一审人员写在description.txt上，请复制过来
- title：注意这里填写的是文章的title，不是GSE界面的title！注意不要title不要加句号,可以调用函数自动生成
- authors：使用内置函数获取，script中的引号内填pubmedID，注意内置函数获取的作者名可能存在乱码，需要手动检查更改！
- accessionNumber：这里填写GSE开头的编码,可以在pubmed网站中文章下面的Associated data这一栏获取，一般GSE三个字母后面有5位数。
- pubmedID：填写pubmed编号，pubmed网站上文章题目下面的PMID就是。
- keywords： 使用内置函数获取，script中的引号内填pubmedID
- abstract：把文中的abstract一段复制下来，注意不要多了或少了。不要填写GSE界面的summary!
- sourceID：出现在pubmed网站中文章题目下方。DOI的网络连接，如一篇文章的DOI为10.1038/nature12172，那么它的网络链接为https://doi.org/10.1038/nature12172

##### libraryPreparationMethod/sequencingPlatform/clusteringMethod/biomarkerDerivationMethod/genomeBuild/annotation这6个字段有控制字段可查看参考，

##### 调用my_inspector.show_controlled_vocabulary()这一命令即可查看字段。

- libraryPreparationMethod：是指细胞测序所使用的技术，可在文章中查找；也可以在文章相应的GEO网址中（即在GEO网站首页输入GSE编码即可）查找。文章中的技术，使用my_inspector.libmethod_keywords调出关键词： 10x chromium(注意不要随便更改大小写！), drop-seq, microwell-seq, C1 Fluidigm, inDrops, Smart-seq2, Smart-seq, CEL-seq, CEL-seq2, MARS-seq, msSCRB-seq, SCRB-seq基本上都出自于上面几种测序技术。

- sequencingPlatform：这里填写测序平台。可以在文章相应的GEO网址中找到。如，Illumina HiSeq 2500，Illumina HiSeq 500，Illumina HiSeq 2000等。

- clusteringMethod：这里填写聚类分析的所使用的方法，可在文章中查找。聚类分析常用方法：k-means, affinity propagation, mean-shift, spectral clustering, Ward hierarchical clustering, agglomerative clustering, DBSCAN, Gaussian mixtures, birch.
![](images/figure-1.png)
  

- 想要更详细的了解可以参考以下网址：https://blog.csdn.net/ztf312/article/details/97951928

- biomarkerDerivationMethod：是指marker gene的算法，一般在文章的method里面有，是找cluster下游的特异基因的方法。一般是t-test或者wilcoxon之类的，尽量在文章中找到对应的marker genes的算法。

- genomeBuild：可查看文章相应的GEO网址中是否有相应字段。人为hg/GRCh，小鼠为mm/GRCm这类格式，其他物种可以填notAvailable。

- annotation: 是指基因的注释信息是什么,在GEO网站搜索，大多数时候是基因的版本号，但也有出现其他信息的情况，似乎被作者当作note信息来使用。

- numberOfCells:填写这个subdataset中的细胞数量，以TPM中的细胞数为准。可以参考cellAnnotation中的cellID这一列中的细胞，有自动函数可供运行。需要注意：如果运行了To_TPM的函数，需要在函数运行完成重新获取细胞数，因为有部分细胞可能被筛掉了

- numberOfSamples: 填写这个subdataset中的样本数量，根据cellAnnotation中的sampleID这一列中的种类计数填写。注意，如果sampleID为notAvailable，需要填写为0。有自动函数可供运行

- ```python
      number_of_cells = my_builder.read_template_tsv(cellAnnotation).shape[0]
    	metadata['numberOfCells'] = my_builder.get_number_of_cells()
      #运行完To_TPM的函数后需要重新获取
    	metadata['numberOfSample'] = my_builder.get_number_of_samples()
  ```

- numberOfGenes: 填数字，不是字符串。这里填写该part的基因，以TPM中的基因数为准。不包括\"ERCC-\"开头的基因，可以从TPM矩阵的column得出。注意不要把TPM矩阵的cellID, normalizationMethod列也给统计进去了！

- fastqURL：在EBI网址上查找文章名字，然后点击相应链接，查看data信息，链接就会跳转到一个有大写字母PRJNA和一串数字结尾的地址，如 ：https://www.ebi.ac.uk/ena/data/view/PRJNA542142其实只要网页里面有文章的fastq相关信息即可
- figureURL：填写文章的摘要图网址。对于明确表示有graphic abstract 的文章，我们需要把这张图放在展示页面上，如果没有graphic abstract，那么放文章的第一张图。可以在文章页面访问原图，使用原图链接，或者访问杂志网站，使用杂志提供的图片链接。图片链接结尾一般是*.jpg .png .gif之类的文件形式。链接需能在浏览器中打开，但不可使用只自动下载的图片。
- codeURL: 如果作者提供了代码，需要把链接填写在这里。一般会在文献中的codeAvailability中找到github链接或者dropbox链接，里面有时会有cluster信息，请仔细查看。当没有内容可以填写的时候，需要填写notAvailable。
- dataURL: 非GSE数据没办法填写GSE开头的accession number的数据集填写此项。数据来源有多个，或不是从GEO下载的都需要填写上非GEO的那个数据来源。如果这一项没有可以填写的内容时，填写notAvailable。
- isFigurePublic：填True or False, 是否对所有网络均公开可见，如一般abstract中的figure为公开的，就填写True。
- taxonomyID：可查看文章相应的GEO网址中的sample里的信息，一般人填9606， 鼠填10090（只用填写了这个才能生成geneannotation）
- journal： 使用内置函数获取，引号内填pubmedID，如无法获取，使用命令my_inspector.journal_keywords调用journal的列表查看，列表中没有该杂志名称的话就自己填写。
- citation: 指引用次数,可调用函数自动生成 metadata['citation'] = my_builder.get_citation(metadata['sourceID'])
- tissue：文中选填,填为list格式。需要在https://www.ebi.ac.uk/ols/ontologies/bto 这个网址查询是否有填写的tissue。tissue这个字段的设置是为了之后做筛选时能快速筛选出相应组织的所有数据集，例如melanoma这种黑色素瘤疾病，我们可以填写skin为tissue。不要在tissue中填写细胞，如oocyte卵母细胞，不应该填写到tissue里面，但是根据文章题目得知文章是研究多囊卵巢综合征的，这时就可以把tissue填写成ovary卵巢。注意：当使用细胞系且皮下注射培养肿瘤时，先都填写成notAvailable即可。但如果能够判断出具体器官/组织，如使用小鼠建造乳腺癌模型，最终取整个breast作为样本，则可以认为breast为tissue中应该填写的内容。
- tissueOntology: 不填写，向下运行代码自动生成
- UBERONTissue: 填写在UBERON这个库里找到对应的tissue。https://www.ebi.ac.uk/ols/ontologies/uberon
- UBERONID: 填写UBERSON里面tissue对应的ID，有自动函数可供运行。
- clusterAvailability：填True or False，意思是能否找到对应的cluster信息
- otherDataType：该字段被存储为list类型，这个字段里面填写文献中是否有出现除了scRNA-seq的数据类型如TCR, BCR, CyTOF, CITE-seq,spatial transcriptomics，total-seq，REAP-seq，一般可以在以上数据类型中挑选。已形成controlled vocabulary在inspector中可以使用Tab键查看。注意：当TCR等数据类型的样本是单独的，跟scRNA-seq的样本不一致，可以加个“_otherSample”的tag，例如：['TCR_otherSample']。

### 以下字段为文章的研究主题

- disease：填True or False

- isDiseaseTreated: 这个字段只在disease为True的时候填写，填写True or False。当这个数据集中使用的sample，包含有treated的sample，那么填写True，反之填写False，disease不为True的这个字段，空着即可，不需要填写任何内容。

- methodology：填True or False （True 仅包括本身研究测序方法的文章）

- cancer：填True or False

- neuroscience：填True or False

- developmentalBiology：填True or False （发育生物学，如细胞的分化）

- immunology：填True or False

- cellAtlas： 填True or False （遗传图谱）

- tSNEAvailability：填True or False, 意思是能否找到tSNE的坐标信息

- isBadtSNE: 填True or False,由6中的tSNEplot画出图形判断

- UMAPAvailability: 填True or False,意思是能否找到UMAP的坐标信息，同样需要发邮件

- isBadUMAP: 填True or False,由6中的UMAPplot画出图形判断

- isCultured：填True or False，意思是scRNA-seq所用的细胞是作者自己传代培养的细胞系（True）的还是原代细胞（False）。

- isTPMNotAvailable：填True or False。这个字段的意思时问这个数据集中的TPM矩阵是否是真正的TPM矩阵？因为：Some articles provide norm matrix only and cannot generate TPM (we can only treat the norm as if it is TPM)，找不到真正的TPM矩阵的时候填上True。但在矩阵的normalizationMethod这一列里要标注清楚'Copied from norm'，表示这个不是TPM矩阵！

- diseaseOntology：在https://www.ebi.ac.uk/ols/ontologies/doid中寻找disease_name。
![](images/figure-2.png)

- ###### 若数据集为整合数据集，即该数据集涉及到之前已经标注过的数据集时需要填以下字段

​     isIntegratedDataset:填True or False。

​     subset: 填accesionNumber/pubmedID 对应的number号，No_x_x，运行以下两个方法查找number号和相应的description

```python
#输入文章的GSE号码，查看是否之前做过，对应的number号是什么
my_builder.search_dataset_desc_by_accession('GSExxxx')

#输入文章的pubmedid号码，查看是否之前做过，对应的number号是什么
my_builder.search_dataset_desc_by_pubmed_id(0)
```



- （当cancer为True时需要填写）cancerDescription：按照script脚本里面的要求填写，文章不是cancer相关可以不填

- ```python
  cancerDescription = {}
  cancerDescription['TIL'] = '' # 填 True or False.Tumor Infiltrating Lymphocyte肿瘤浸润淋巴细胞的缩写,
  cancerDescription['TIM'] = '' # 填 True or False.tumour infiltrating myeloid cell肿瘤浸润髓样细胞的缩写,与TIL并称为 tumour infiltrating immune cells (TII),
  cancerDescription['TME'] = '' # 填 True or False.Tumor Micro-environment肿瘤微环境的缩写(non-immune cells, which we mean by cancer\ fibroblast, etc),
  cancerDescription['NCIBodyLocation'] = '' # 从给的keywords选填, 如: Head and Neck,没有就填notAvailable
  cancerDescription['NCIBodyLocationSubtype'] = '' # 根据NCIBodyLocatio对应的keywords选填, 如: Hypopharyngeal Cancer,没有就填notAvailable
  cancerDescription['TCGAStudyAbbreviation'] = '' # 从给的keywords选填, 如: LAML,没有就填notAvailable
  cancerDescription['TCGAStudyName'] = '' # TCGAStudyAbbreviation对应的全称如: LAML对应的全称为: Acute Myeloid Leukemia,没有就填notAvailable
  	# 运行 my_inspector.nci_cancer_typing 查看 NCIBodyLocatio的keywords
  	# 运行 my_inspector.tcga_cancer_typing 查看 TCGAStudy的keywords
  ```

![](images/Leukocytes.png)


### 对于各字段的说明：

- disease：是指文章研究内容是否与疾病有关，例如研究某种疾病、从疾病患者采样等，但对于一般性的研究某一通路、细胞等作用，最后认为可能与某种疾病有关时需要多加判断，准则是此篇文章出发的目的和主题内容是否是与疾病相关，此外，cancer必为disease；
- methodology：此为高出错字段，应更加注意。方法学是指文章开发了某一新的算法、分析方法、技术手段或者比较现有算法并进行评价的。很多文章习惯性采用“我们使用了新的策略”这样的表述，这种并不是方法学文章的特征，也并不是用了某一算法、框架的就是方法学，准则是“新”的方法以及文章主旨；
- cancer：很容易理解，研究主旨是关于癌症的；
- neuroscience：研究主旨、研究样本关于神经生物学或神经组织/细胞；
- immunology：研究主旨、研究样本关于免疫学或免疫器官/细胞；
- developmentalBiology：研究的是某一个体/器官/细胞类型/组织的时间跨度下的发展变化，关键字如发育、衰老、胚胎、分化等，此外，使用胚胎的文章大多可归为发育；
- cellAtlas：对某个个体、器官、组织、系统。进行全面普遍的研究，即关注的问题不是某一特定的细胞类型或组成的功能与变化，而是清楚表达其内部组织与层次关系，可以类比为“参考基因组”这样的概念，不是为了研究某个基因或某个人，而是为了提供可参考的人类基因组。也会有文章进行全面测序和分析后，只抓住某个点阐述，此时也仍属于cellAtlas，判断标准是测序的样本和数据是否包含了其采样的全部，比如有的取了肝组织，但只测肝内免疫细胞，则不符合要求，应该包括基质细胞、上皮细胞等等。

### 关于treatment的填写及说明
- isDiseaseTreated：填True or False, 当disease为True时，且isTreated为True，且treatmentMethod包含drugorChemicalTreatment时，需要填True。其余情况填False。

- isTreated：填True or False，指该研究是否对用于scRNA-seq的细胞/样本做了药物/化学/治疗处理、基因表达干扰(主要为基因敲除/敲降/敲入)、病毒/细菌感染和其他非药物性条件性处理(如小鼠高脂食物/低氧环境等)。可在文章Method部分或该数据集对应的GEO数据库(GEO-sample-characteristic-treatment/treatmentprotocl)检索相关信息。isTreated是study层级字段，study涉及到处理的所有这个study下的所有数据集就填True。涉及到每个dataset，有处理的再填相关的4个treatment key。没有处理的四个treatment key就留空。如果study不涉及到处理，那所有数据集就填False，四个treatment key就留空。isTreated填True时，需要进一步填写下面四个字段；isTreated填False时，则下面四个treatment字段留空。

- treatmentMethod： 在下列4种处理类型中选择一种或多种进行填写，即drugorChemicalTreatment/ genePertubation/ infection/ conditionsTreatment。
  ① drugorChemicalTreatment(药物/化学/治疗处理) eg: nivolumab/IL-13/anti-PD-L1 antibody
  ② genePertubation (基因表达干扰，主要是基因敲除/敲降/CRISPR) 
  ③ infection (病毒/细菌感染) eg: Influenza B virus/SARS-CoV-2/Salmonella 
  ④ conditionsTreatment (非药物/治疗的外界条件性处理), 常见的情况包括小鼠喂食高脂高热量食物/缺氧环境/超声波处理/辐射，细胞共培养等。 eg: NASH-Model Diets/Hypoxia

- treatmentName：此处维二维列表，与treatmentMethod依次对应。指Name for something used as a treatment。注意尽量填写药物/化学试剂/病毒/细菌名称的全称，避免缩/简写; 若treatmentMethod为genePertubation，这里写被改变表达的基因名。

- treatmentProtocol：具体treatment的实验描述。与treatmentMethod依次对应。
若treatmentMethod为genePertubation，这里写基因表达干扰的种类KD/KO/cKO/KI，以及siRNA/CRISPR, crossed等基因敲降/敲除/条件性敲除/敲入等方法的名称。eg: KO-CRISPR/ KD-siRNA；若treatmentName多余两个，需要增加key来填写。

- treatmentObject：指treatment的对象。根据文章信息选择性填写Cell；CellLine；healthHuman；patient；normalMouse；diseasedMouse；KO-mouse；cKO-mouse；KI-mouse；PDX-diseasedMouse；normalSpieces；diseasedSpieces。
cellLine指对细胞系做treatment后进行scRNA-seq；
cell指直接对细胞(非活体)做treatment，这里的细胞包括来自于健康/疾病人类，小鼠/疾病小鼠模型或从机构/实验室获取的细胞等。
PDX-diseasedMouse, PDX即人源性组织异种移植(patient-derivedxenografts , PDX)，指对移植人源组织的小鼠做处理，常见情况是对小鼠移植/皮下注射人源肿瘤/肿瘤细胞系。

##### 注意！！！若treatmentMethod为多个，则一定要按treatmentMethod的填写顺序来填写对应的treatmentName/Protocal/Object内容，因为他们是一一对应的关系！！！



### markergene

markergene 的填写一般是在文章supplementary information 里面查找存有markergene的excel表格。然后把excel表格里的markergene信息依次填写进字典中。如果cluster是自己计算的，markergene是作者提供的，有时候会出现inspector报错说markergene里面的cluster和cellAnnotation里面的cluster不对应，这种情况下，我们只需要把作者给的markergene塞到unstructuredData里面相应位置就好了，只要知道报错原因就可以了。 可以参考script脚本的指导和新手代码教程填写



## 3. cellAnnotation:

这里需要查看一些矩阵里的信息来完善cellAnnotation表格，需要用到代码 注意：没有的数据，不要留下NaN，若找不到信息就填成notAvailable。

- 填写CellID: 一般在Matrix_rawCounts或者Matrix_normalized有对细胞的编号，只需要取出后安进cellAnnotation表格中的cellID这一列就可以了。

- 填写meta_字段：可以在读取了sample信息后查找是否有值得填写的字段，一般可以查看文章对应的GEO网址里面characteristic一栏中的信息来填写这一部分，注意需要与细胞一一对应。在填写sampleID的时候，需要注意GEO网站上的sampleID是GSM开头的，EBI网站上的sampleID是ERS开头。

  注意：cellAnnotation中"meta_"开头的字段名称不能包含英文的"+"，例如meta_test+test就不可以

  

- sample表格中的信息如何与CellID找到关系来填写cellAnnotation表格里的内容？ 一般是通过sample表格中title这一列来寻找关系的：

  1） title中的信息与cellID信息相同，只是顺序不同: 可以借助循环代码历遍表格，利用当title中信息与cellID中信息相同时，才能填入cellID这一行的其他相对应的信息为条件，填入表格信息。

  2） 如果title中的信息和cellID不同，需要在sample这个表格中另外找能够跟cellID对应 起来的信息。再进行1）中操作。

- 填写clusterName和clusterID（tSNE and UMAP）:

  1） 首先要在文章中的supplemental information栏下查找作者是否给了是否有相应的聚类信息（包括clusterName/clusterID，tSNE1/2, UMAP1/2等）。

  2） 还可以在读取作者提供的矩阵（文章对应的GEO网址下载的）中查找。

  3） 作者还喜欢在文章中的CodeAvailability中提供github链接或者dropbox链接，里面一般会有metadata和cluster信息。

  4） 最后也没找到clusterName/clusterID的可以先填写上notAvailable，之后运行我们自己的计算脚本，自动生成为数字编号的clusterName/ID、tSNE1/2、UMAP1/2。这部分代码出现在template/script.ipynb中的6.使用脚本自动生成其他项中的6.4，同时cellAnnotation的表格中会多出来两列：clusteringMethod 和 clusterName_scibet。



​       5）注意clusterName中不可以出现”.“,要用”_“来替代。

​       6）若tsne和umap作者也没有提供，跟cluster一样，实在不行可以运行自己的脚本自动计算。6.3中的代码生成2D（第一行代码） 和3D的图（第二行代码）

​       7）填写cellOntologyName和cellOntologyID：这两项与clusterName是相对应的 1） 如果文中提供clusterName等相关内容，cellOntologyName/ID可以在https://www.ebi.ac.uk/ols/index网址搜索细胞信息，并输入与clusterName的cellOntologyName和ID (id现在已经不需要填写，运行6.5代码根据cellOntologyName自动生成)如果找不到一样的cellOntologyName，就在关系树上向上找最接近的cellOntologyName。

##### 若作者提供了clusterName，可以利用以下两个函数简单搜索相似的cellOntology，搜索结果用于参考，具体以文章中提到的clusterName在EBI网站中找到的cellOntology为准

```python
#搜索已有数据集中相似clusterName填的cellOntologyName
my_builder.search_ontology_by_cluster(name='') # 填clusterName 如 'T cells'

#根据clusterName从所有cellOntology中搜索相似的cellOntologyName
my_builder.search_ontology(name='') # 填clusterName 如 'T cells'
```

第一个函数返回结果如下图所示：

![](images/figure_14.png)

如图，最左边为相似度，返回的similar_cluster为数据库中与输入的clusterName相近的cluster, ontologyName为数据库中对应的cellOntologyName

第二个函数返回的结果相似，不过会直接返回数据库中相似的cellOntologyName



​      8） 如果文章没有找到clusterName，那么就只能在文章中寻找进行聚类分析的是什么细胞，有时候只能找到这篇文章是研究**细胞的，如骨髓细胞，那就只查找骨髓细胞相应的cellOntology信息并全部填上即可。目的就是填上就好，能填就填，轻易不要填notAvailable，更不可以空着。

9) 注意：cellAnnotation中需要至少要有meta_tissue或meta_sourceName两者间任意一列。这两列的信息首先可以根据sample信息来填，大部分作者都会给出tissue或sourceName的信息,如果这里找不到的话可以去文中看一下数据集中的细胞来源是什么，比如哪些样本是取自某个tissue或来自于某个cellLine的等等，如果实在没有来源信息可以填notKnown


## 4. Matrix:

###注意！！！如果作者只给了normalize矩阵，而且count含负数，需要及时联系管理员

### 注：当文献里面细胞数目超过1 万时，需要把矩阵存成mtx格式，一旦存了mtx格式，所有矩阵都需要存成mtx格式。

### 新加入的函数支持对作者给的原始数据进行筛选。如果筛选后的细胞数目还是过大，如几十万或上百万这种，需要及时联系管理员。

### 什么是filtered matrix，什么是没有filter过的矩阵？

- 根据作者在文章中提供的tSNE/UMAP等图的注释信息，一般可以得到降维图中的细胞数量的信息，如果矩阵中的细胞数目和文章中不相符，即为没有filter的矩阵，一般这种矩阵极大，有时有几十万甚至几千万细胞，这是首先需要根据原文中作者对细胞，基因的筛选条件，通过脚本提供的函数尝试进行筛选才可能处理。

### 1. filtered 矩阵怎么处理？

- Priority: filtered TPM> filtered norm matrix > filtered raw_Counts matrix
- a) 提供了filtered TPM矩阵是最好的情况，用这个处理就好（若作者只提供TPM矩阵，需要存进norm和TPM里面才行，因为TPM也是作者标化过的矩阵）
- b）当没有filtered TPM 但是提供了filtered norm matrix 或者 filtered raw_Counts matrix，使用相应的filtered norm/raw 转置成TPM，当norm和rawCounts同时出现时，优先使用norm矩阵转置成TPM.
- c) 注意：norm矩阵的normlization method需要写清楚，当文章中没有详细描述的时候，需要去GEO的网站看一下GSE相应内容，若是两个地方都没有描述，需要自己尝试是否是熟知的标化方法，即假定norm矩阵的标化的方式为熟知的log2(TPM+1)，通过去log并且减去1的计算后，查看矩阵的行和是否为100万。如果是100万，那么就是norm矩阵的normalization Method假定的标化方法log2(TPM+1)；如果不是100万，需要另外多尝试几次其他的标化方法。有的时候norm的标化方法为FPKM, log2(TP10K+1), log2(TP20K+1), etc. 自己多尝试。
- d) 恢复不成TPM的norm矩阵，除了要把norm矩阵填写好，还需要把norm矩阵原封不动复制到TPM矩阵里面，如果norm恢复不成TPM并且存在raw，可以使用raw矩阵，根据norm矩阵筛选细胞和基因后生成TPM，TPMNormalizationMethod: TPM from raw,filtered as norm; 如果没有细胞基因不需要根据norm过滤，则TPMNormalizationMethod填写成TPM from raw（但是当多个数据整合后作者自己处理了矩阵的数据时，建议直接从norm copy矩阵进TPM，不需要自己处理rawCounts）。并且在normalization Method里面写清楚，具体参考下方***如何填写normalization method?
- e) 从raw_Counts生成TPM只需要运行自动函数即可。

### 2. 作者仅提供了rawcount矩阵该如何处理？

- 当作者仅提供了rawcount时，需要先根据文章QC信息运行以下函数对细胞，基因进行筛选，然后自动生成TPM。

- 以下命令为由rawcount矩阵根据文章QC信息生成 TPM的脚本：

注意！运行这个函数前需要填写cellAnnotation/geneAnnotation，cellAnnotation至少应该填写了cellID列，其他可以先留notAvailable；geneAnnotation应该已经填写完毕。当raw_count存为mtx格式的时候，检查是否也同时保存了配套的cellID_rawCounts.tsv/genes_rawCounts.tsv文件，否则会报错。
另外还需确保cellAnnotation的cellID列和rawcount的细胞名对应。geneAnnotation中geneSymbol/ensemblID/geneID中的某一列，以及genes列raw_count中的基因名对应。

- ```python
  cell_filter = {
      'min_genes': None, #细胞最少表达的基因数
      'max_genes': None, #细胞最多表达的基因数
      'min_counts': None, #细胞最少的count数
      'max_counts': None, #细胞最多的count数
      'max_percent_mito': None, #细胞中线粒体基因所占比重的最大值
      'max_n_genes_by_counts': None, # (genes_by_counts的意思是对细胞表达的基因根据total count进行normalize之后所得到的"标准化"表达量, 通过这种normalize后所有细胞的total count都相同)
  }
  gene_filter = {
      'min_cells': None, #基因最少需要在多少细胞中表达
      'max_cells': None, #基因最多需要在多少细胞中表达
      'min_counts': None, #基因最少的count数
      'max_counts': None, #基因最多的count数
      
  }
  my_builder.toTPM(cell_filter=cell_filter, gene_filter=gene_filter)
  #如果报index error,可以加入参数gene_filter = xxxxx，根据作者给的基因类型，手动指定基因类型:geneSymbol/ensemblID/geneID
  #当libraryPreparationMethod不是常见的10x chromium, Smart-seq2等方法时，需要手动上网查询文中的建库方法是否为全长测序，并加入参数is_full_length=False或者True
  ```

当最后画出的t-SNE/UMAP图聚类情况不好的时候，可以尝试使用默认参数筛选重新生成TPM: 'max_n_genes_by_counts': 2500, 'max_percent_mito': 5，注意如果作者没有提到筛选，或者筛选条件不明轻易不要使用此参数



  运行该命令需要首先填写unstruc文件中metadata部分的libraryPreparationMethod，如果libraryPreparationMethod在我们给定的标准字段（见填写unstructuredata的注释）中，则直接运行以上命令，否则需要判断是否为全长测序，并在函数中加入参数is_full_length = True 或者 False.

  如果raw counts存为sparse matrix，也可运行该函数由raw counts得到TPM

  根据文章的QC方法描述，填写cell_filter和gene_filter参数

  注意：当填写了cell_filter/gene_filter的QC信息的时候，生成TPM矩阵的同时会在cellAnnotation/geneAnnotation自动生成一列"filtered", 需要注意检查该列为True的细胞/基因是否可以和TPM矩阵的行/列数量对应的上，如果对应不上后续会报错

- a) 当作者没有提供任何 filtered 矩阵， 同时又没有任何QC信息的时候，只能先找cluster信息；如果找得到，就把最原始的没有filter过的raw_Counts或norm存储到expression_rawCounts.tsv/mtx 或 expression_norm.tsv/mtx，并且根据cluster信息把细胞筛选后存储到TPM, **cell Annotation和gene Annotation中的细胞与基因始终与TPM保持一致**，所以当TPM为mtx格式时也无需另外存储CellID和GeneNames.

- b) 如果没有cluster的信息，或者根据QC的信息过滤的矩阵细胞数还是很大的时候，首先查看细胞数量：不超过10万的可以尝试自动运算脚本；超过10万的需要联系管理员，查看数据集的优先级，优先级不高的可以直接放弃，以后再做。

### 3. 如何填写normalization method?

下载的normalized矩阵和生成的TPM矩阵都需要填写一列normalizationMethod。

请务必注意：在矩阵中填写的基础上，也需要在unstructuredata里面的TPMNormalizationMethod字段中统一填写TPM矩阵的normalization方法

1. Normalized矩阵：
   - 一般是下载得来，需要在文献中找出normalized Matrix是怎样被标准化的，有的是FPKM,有的是RPKM,还有log2（TPM+1）等等形式，载入normalizedMatrix的时候需要在第一列标明normalizedMethod。
   - 格式： 如，FPKM。（直接填写原文作者把rawcounts标准化的方法，不要添加其他字符。）
2. TPM矩阵：
   - 如果由rawcounts矩阵转化而来，填写：TPM from raw counts
   - 如果由normalized矩阵转化而来，填写： TPM from FPKM （根据normalized矩阵的方法而改变，如：TPM from log2（TPM+1） ）
3. 如果遇到无法转换成TPM的，TPM的normalizationMethod一定要强调这个不是TPM！并且一定要很详细的注明这个矩阵的标准化方法是什么, 如：矩阵不是TPM! 是TMM矩阵：TPMNormalizationMethod: directly copied from TMM, not TPM
   
4. mtx格式的矩阵：
   - 当Normalized矩阵存储为mtx格式的时候，矩阵中不能存储英文字符，只能存储数字
   - 遇到这种情况，需要在unstructuredData里面额外自己添加字段：normalizationMethod（Normalized矩阵）来存储矩阵的标化方法


### 4.矩阵里面的"ERCC-"基因该怎么处理？

- 基本原则：作者给的矩阵(raw/norm)不进行筛选，但是生成的TPM需要删除“ERCC-”这种内标基因（注意区分ERCC-和ERCC,内标基因带"-"）。删除"ERCC-"基因后需要重新把TPM矩阵norm到100万（除以行和后乘以100万），然后重新运行所有自动计算（不要忘记tSNE/UMAP坐标等的计算，作者给了的情况除外）。
- 注意： 极少数情况下，作者只提供TPM矩阵，且TPM中存在"ERCC-"基因，这种情况下需要：
  - 1. 把作者给的存在"ERCC-"基因的TPM矩阵填写进norm矩阵里面。
  - 1. 生成一个新的没有"ERCC-"基因的矩阵存进TPM里面，用于下游计算使用。
  - 1. 新的pipeline在生成TPM的时候会自动去除“ERCC-”基因，但仍需注意检查

### 5. 注意：

**若作者只提供了TPM 矩阵：**

- 需要把矩阵存到tpm和norm矩阵里面，normalizationMethod分别填写:TPM from author; TPM

**Cell number, 即有效细胞数的判断：**

- 跟TPM保持一致即可。

**为什么要用 TPM：** 在大数据中查询基因表达时，不同数据集在同一个scale上便于比较，因而要有统一的标准。

**为什么尽可能不用 rawCounts 生成 TPM：** Matrix_normalized 中数据往往是对 Matrix_rawCounts 数据做出某些处理后所得，尤其是经过 gene or cell filtering。因此，通常 Matrix_rawCounts 可能有更多的 cell，即存在无效细胞。因而 Matrix_rawCounts 列表就可能和后面的 cellAnnotation 不对应，故后续无法用其向 cellAnnotaion 表格中插入数据。

**判断 downloaded data 是 Matrix_rawCounts 还是 Matrix_normalized：** 根据测序原理，检测结果 Matrix_rawCounts 中一定全为整数，有负数则不是 Matrix_rawCounts。当算法为pseudo alignment（kallisto等，这种算法越来越受欢迎）时，会出现小数。

**判断是否直接使用 Matrix_rawCounts 生成 TPM：** 当且仅当文章未提供 Matrix_normalized 数据，可以直接用 matrix_rawCounts 生成TPM。

**判断下载的 Matrix_normalized 是否能还原成 TPM：** 作者在文章 method、analysis 部分可能提及使用 multi-step normalization strategy，诸如去除批次效应 reduse batch effects, use ComBat method；使用中数标化等待. 一般是还原不成TPM 如若不确定直接下载的 Matrix_normalized 数据是否经过 logarithm transciption，则假定一种标化方法尝试还原成TPM，并检查TPM的正确性。还原不回去的，使用rawCounts矩阵根据norm矩阵筛选细胞和基因后生成TPM,TPMNormalizationMethod: TPM from raw,filtered as norm; 如果没有细胞基因不需要根据norm过滤，则TPMNormalizationMethod填写成TPM from raw（但是当多个数据整合后作者自己处理了矩阵的数据时，建议直接从norm copy矩阵进TPM，不需要自己处理rawCounts）。

### 检查TPM矩阵正确与否方法：

TPM中横行代表基因，纵列代表细胞。对于任意单个细胞，当其对应横行中的基因值相加和为1000 000 (10^6)，则TPM正确。如果文章提到使用UMI, 那么CPM（也叫RPM） = TPM, 行和都是一百万，M代表million。但是有些实习生在从norm矩阵生成TPM时算不出TPM，但是强行除以行和并乘以一百万，导致行和依旧为一百万，这种情况需要仔细检查。

### 使用 Matrix_rawCounts 生成 TPM：

注：当需要用Matrix_rawCounts自动生成TPM时需要先填写unstructuredData中的libraryPreparationMethod。 在文章作者给出 Matrix_normalized 情况下，判断是否可以直接用 Matrix_normalized 生成 TPM，或者仍需使用 Matrix_rawCounts 部分数据生成 TPM 的方法：

1. 作者于文章 data analysing/processing 中明确指出 median normalize (to zero)，或者 Z-Score、TMM，以及一些依赖函数库的结果不能使用 Matrix_normalized 生成 TPM，要根据 matrix_normarlized 中的的 cell ID 挑选出 Matrix_rawCounts 中的的有效 cell ID，使用有效cell ID生成仅包含有效细胞的新Matrix_rawCounts，用其生成 TPM。
2. 作者文章 data analysis/process 部分未发现不可用 normalized data 生成 TPM 的条件，则可做一个 Matrix_normalized。如果该 Matrix 里有负数，则作者 normalization 方法可能为 median normalize (to zero)，或者 z-score 等等。此时同样需从下载的 Matrix_rawCounts 中根据 cell ID 挑选出 Matrix_normalized 中使用的有效细胞，再使用筛选细胞后的新 Matrix_rawCounts 生成 TPM。

### 从 logarithmic transformed Matrix_normalized 到 TPM：

若作者明确提供的 normalized，标化的形式如log2（TPM+1）或者loge(TPM+1)等，则需逆推 normalized矩阵生成 TPM。 logaN, a = 2, 10, e..., TPM = a^N /1e6，(1e6 = 1 million), norm 矩阵的normalizationMethod: loga(TPM+1) 特例：如果文章写明使用 logarithmic transformed data，但并未指出是何种log变换，尝试用log2、log10、ln等等变换后 Matrix 每行基因和均不为1000 000，且每行基因的和相近，则可以直接在 Matrix_normalized 基础上进行归一化，即每个基因除以该行基因的和，再乘上1000 000。

### 从FPKM, RPKM到TPM的转换：

接将 Matrix_normalized这个 矩阵除以 行的和 乘上 1000 000，这样得出来的就是 TPM 矩阵了。 示例代码：（需要根据不同的情况变换，仅作示例）

```python
df_downloaded = pd.read_csv ('../downloaded_data/GSE_supplementary_data/GSE75413_genes.fpkm_table.txt.gz', sep = "\t",index_col = 0)                  
#读取矩阵
df_downloaded.head()   
#显示矩阵前前5行出来看看

df_norm = df_downloaded.T  #行和列交叉互换，即行变成列，列变成行
df_norm.head()

fpkm_array = df_downloaded.T 
b = np.sum(fpkm_array,axis=1)
df_tpm = fpkm_array / b.values.reshape(-1,1) * 1e6          
#1e6代表1 million
```

## 5. 生成的其他文件：

1. TPM需要从rawcounts生成时，运行6.1（注意：只有当libraryPreparationMethod填写完了之后才能运行成功，需要从文章中找到作者提供的cell/gene的QC信息）

2. geneAnnotation文件填写：在script.ipynb中的6.2可见。（注意：只有taxonomy填写了物种的编码之后才能成功生成）

   该文件需要填写geneSymbol,ensembleID, ensembleID所对应的ensemble版本以及geneID。geneSmbol—ensembleID—geneID之间的相互转化通过以下命令实现：

   ```python
   my_builder.list_gene_ref_files(tax_id=9606) #查看指定tax_id的物种可供选择的参考文件
   my_builder.show_gene_ref_file('ensembl_gene_id---external_gene_name.tsv.gz', tax_id=9606) #查看具体的参考文件内容
   my_builder.ensembl_version #查看当前用于转换的ensembl版本号
   # eg1:
   my_builder.convert_genes(['CD4', 'ACTB'], ref_file='ensembl_gene_id---external_gene_name.tsv.gz' , tax_id=9606) 
   #基因以list形式填入，ref_file选择转换对应的参考文件，tax_id填写物种taxonomyid
   #该函数输出为转换后的基因，形式为list, 例如['ENSG00000010610', 'ENSG00000075624']
   
   # eg2:
   my_builder.convert_genes(['ENSG00000210077', 'ENSG00000209082'], ref_file='ensembl_gene_id---external_gene_name.tsv.gz' tax_id=9606)
   ```

   目前支持的物种taxonomyid为：10090，10116，6239，7227， 7955，9031，9541，9598，9606，9823

​       支持的转换类型为;

​       geneSymbol<--->ensemblID ，reference文件: ensembl_gene_id---external_gene_name.tsv.gz
​       geneSymbol<--->geneID ，reference文件: external_gene_name---entrezgene_id.tsv.gz
​       ensemblID<--->geneID ，reference文件: ensembl_gene_id---entrezgene_id.tsv.gz

​       geneID为NCBI的"Entrez Gene ID"

​       将原基因及转换基因填入geneAnnotation中，填写的列包括：

​       

```python
df_gene['genes'] = #genes (与raw/norm矩阵保持一致，可以与geneSymbol、ensemblID、geneID列相同)
df_gene['geneSymbol'] = #基因名称
df_gene['ensemblID'] =  #ensemblID
df_gene['geneID'] = #NCBI的"Entrez Gene ID"
df_gene['ensemblIDVersion'] = #ensemblID的版本号

#特殊情况需要手动加入以下列
df_gene['filtered'] = #如果作者提供的为normalized矩阵，且genes包含'ERCC-'之类的需要去掉的基因，这时在geneAnnotation矩阵仍填所有的基因，但要手动添加一列'filtered'需要保留的gene填为: True，不需要保留的gene填为: False，TPM矩阵要删除相应的列（"ERCC-"开头的基因），normalized矩阵保持不变
```

最后保存为geneAnnotation.tsv矩阵

3. 填充publication_consistend_markers.json

​       填写这个文件需要仔细阅读文章，填写文中提到的某种细胞类型和其他的marker genes信息

​       （1）某种cell类型的marker

​                参考script.ipynb中的要求填。对于文章提到的不同cell_type（epithelial cell, mast cell, .......）的 marker，应分别按照代码中的要求填为不同的dict,最后由这些不同的dict组成一个cell_type的list。需要注意的是同一celltype下的gene的weight和应为1，且positive/negative marker的weight可根据文章要求做调整。比如strong positive marker可以适当增加weight, strong negative marker可适当减少weight。

如果文中的celltype只有positive或者只有negative marker， 则该marker的weigth填1.0.
某种celltype没有subtype的不用填，直接把该celltype的"subtypes":{}删掉
             

​       （2）文章其他的gene信息

​               参考script.ipynb中的要求填。这里填写一些文章提供的genes信息，但又是非cell type marker的genes，如house_keeping genes、cell_cycle genes等





4.自动计算

（1）当作者或原文没有tsne和umap坐标时，可以运行我们自己的计算脚本，计算tSNE和UMAP的二维和三维， 运算脚本在script.ipynb中的6.4可见。

（2）clustering计算，在6.4中，仅在作者未提供cluster信息的时候运行，记得运行下一部分的判断isBadtSNE，isBadUMAP的代码来判断tSNE/UMAP图上的细胞簇点是否能分得开。

（3）cello：计算CellOClassification，人和小鼠的数据集均需要运行，小鼠的数据集运行可能会比较慢。运行完后可以在cellAnnotation中查找meta_CellOClassification列查看结果

（4）cell_cycle：人的数据集需要运行这项，运行完后可以在cellAnnotation中查找stats_sScore，stats_g2mScore列查看结果

（5）gene_set：人和小鼠的数据集需要运行这项，可以查找processed_data下的gene_set_analysis.json文件查找结果

（6）paga：所有物种的数据集都需要运行这项，可以查找processed_data下的paga.json文件查看结果

（7）cell_phone_db：人的数据集需要运行这项，可以查找processed_data下的cell_phone_db文件夹查看结果

（8）diff_genes：所有物种的数据集都需要运行这项，可以查找processed_data下的Diff_genes.json文件查看结果

（9）pegasus_infer：运行此项前需要确保上一步的publication_consistend_markers.json已经填充完毕



5.画图判断isBadtSNE，isBadUMAP

可以使用以下代码进行tSNE/UMAP绘图和判断tSNE/UMAP图质量，并参考script.ipynb 6.4的说明

```python
#通过以下代码进行tSNE plot绘制，注意运行会自动保存tSNE.png到processed_data的figure文件夹
my_builder.tSNEplot(color_by='clusterName', mode='scanpy', scanpy_size=25, plotly_size=None) #默认按照clusterName对细胞进行分类上色，scanpy_size/plotly_size为点的大小，当细胞过多时可以适当减少size以分开细胞
my_builder.tSNE3DPlot(color_by='clusterName', mode='plotly', plotly_size=None)

#通过以下代码判断tSNE图上的细胞簇点是否能分得开
my_builder.is_bad_tsne(label_name='clusterName') #输出为True或False,分别指代tSNE图上的细胞簇点分得开/分不开
my_builder.is_bad_3d_tsne(label_name='clusterName')#输出为True或False,分别指代3DtSNE图上的细胞簇点分得开/分不开
```

注意判断tSNE图上的细胞簇点是否能分得开的代码若输出为False，需要再看一下图片确认是否真的分不开





6.genes_plot计算，需要根据文章中给出的某些基因的tSNE/UMAP图，画出相同基因在被标注数据中的tSNE/UMAP图，只需要填写文章中使用的基因的即可，格式为list，若是没有基因可选，再尝试使用默认参数T_cell=True, B_cell=True画图。

```python
my_builder.genes_plot([''], house_keeping=True, T_cell=False, B_cell=False)#['']中填写gene名字"
```



7.cellOntologyID，在6.5中运行后根据cellOntologyName自动填写



6.填写README：

- 第一个block里面运行的代码用来赋值和读取readme.json
- 第二个block里面填写信息 Readme['author'] = '' #填写自己的名字拼音，如 Xiaoming Readme['date'] = '' #填写完成日期 Readme['modificationDate'] = '' #填写修改日期 Readme['unfinishedParts'] = [''] #未完成的部分，包括矩阵及cellAnno中的cluster等 Readme['authorComments'] = '' #如果数据分成了几个part，写清楚该part代表什么类型的数据 Readme['otherComments'] = ''

- 最后记得保存

